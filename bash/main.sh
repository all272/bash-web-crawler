echo 'devtesting'
error() {
echo
echo '$errorblank'
echo
echo "


!!!READ!!! There has been an error somewhere in the program."
echo "Would you like to retry or skip updating?
0) Retry
1) Skip"
read servermd5error
if [ $servermd5error == 0 ] || [ $servermd5error == retry ]
then
  echo 'Retrying the update'
  $restartcommand
elif [ $servermd5error == 1 ] || [ $servermd5error == skip ]
then
  echo 'Skipping update'
  exit
else
  echo 'Please enter a valid response.'
  error
fi
}



update() {
echo 'Downloading update script'
curl https://gitlab.com/all272/bash-web-crawler/-/raw/main/bash/updatescript.sh --output updatescript.sh &>> updatecheck.txt
curl https://gitlab.com/all272/bash-web-crawler/-/raw/main/bash/updatescript.sh --output updatescript1.sh &>> updatecheck.txt
curl https://gitlab.com/all272/bash-web-crawler/-/raw/main/bash/updatescript.sh --output updatescript2.sh &>> updatecheck.txt
echo 'Verifying updater'
md5update1=$(md5sum updatescript.sh | cut -d ' ' -f 1)
md5update2=$(md5sum updatescript1.sh | cut -d ' ' -f 1)
md5update3=$(md5sum updatescript2.sh | cut -d ' ' -f 1)
if [ $md5update1 == $md5update2 ]
then
  if [ $md5update2 == $md5update3 ]
  then
    echo 'Update script verified. Starting installation.'
    bash updatescript.sh
    return [n]
    echo '!!!You should never see this!!! If you do press ctrl+c to exit the program and then retry. If this is your third time seeing this please submit a bug report to the Bitbucket.'
  else
    errorblank='Unable to verify update script. This can normally be fixed by retrying the download.'
    restartcommand=update
    error
  fi
else
  errorblank='Unable to verify update script. This can normally be fixed by retrying the download.'
  restartcommand=update
  error
fi
}




md5update() {
echo "Would you like to install the new update?

Yes or No"
 
read updates

if [ $updates == yes ] || [ $updates == y ]
then
  echo "
Redownloading the script.
  "
  echo
  echo 'Deleting old update scripts.'
  echo
  rm updatescript.sh &>> updateremove.txt
  rm updatescript1.sh &>> updateremove.txt
  rm updatescript2.sh &>> updateremove.txt
  rm updatescript3.sh &>> updateremove.txt
  rm updatescript4.sh &>> updateremove.txt
  rm md5server.sh &>> updateremove.txt
  rm md5servercheck.sh &>> updateremove.txt
  echo
  echo
  echo 'Finished deleting old update scripts.'
  echo
  echo
  echo 'Generating md5 checksum for installed script.'
  echo
  md5local=$(md5sum main.sh | cut -d ' ' -f 1)
  md5localcheck=$(md5sum main.sh | cut -d ' ' -f 1)
  echo
  echo 'Verifying the md5 checksum for the installed script.'
  echo
  if [ $md5local == $md5localcheck ]
  then
    echo
    echo 'The checksum for the installed script has been verified.'
    echo
    echo 'Generating md5 checksum for the script on Bitbucket.'
    echo
    curl https://gitlab.com/all272/bash-web-crawler/-/raw/main/bash/main.sh --output md5server.sh &>> updatecheck.txt
    md5server=md5sum | cut -d ' ' -f 1
    curl https://gitlab.com/all272/bash-web-crawler/-/raw/main/bash/main.sh --output md5servercheck.sh &>> updatecheck.txt
    md5servercheck=md5sum | cut -d ' ' -f 1
    echo 'Verifying the md5 checksum for the script on Bitbucket.'
    if [ $md5server == $md5servercheck ]
    then
      echo
      echo 'The checksum for the Bitbucket script has been verified.'
      echo
      update
    else
      errorblank='The checksum for the Bitbucket script has not been verified.'
      restartcommand=md5update
      error
    fi
  else
    errorblank='There was a problem with the md5 checksum generated to tell ig there was an update.'
    restartcommand=md5update
    error
  fi
fi
}





md5local=$(md5sum main.sh | cut -d ' ' -f 1)
md5localcheck=$(md5sum main.sh | cut -d ' ' -f 1)
rm md5server.sh &>> updateremove.txt
curl https://gitlab.com/all272/bash-web-crawler/-/raw/main/bash/main.sh --output md5server.sh &>> updatecheck.txt
md5server=$(md5sum md5server.sh | cut -d ' ' -f 1)
rm md5servercheck.sh &>> updateremove.txt
curl https://gitlab.com/all272/bash-web-crawler/-/raw/main/bash/main.sh --output md5servercheck.sh &>> updatecheck.txt
md5servercheck=$(md5sum md5servercheck.sh | cut -d ' ' -f 1)
if [ $md5local == $md5localcheck ]
then
  if [ $md5server == $md5servercheck ]
  then
    if [ $md5local != $md5server ]
    then
      md5update
    else
      echo 'No update available'
    fi
  else
    echo 'Update check failed'
    #return[n]
    exit
  fi
else
  #return[n]
  exit
fi



echo "

Would you like to install any packages needed before or as they are needed?
0) before
1) as needed"
read packagesbefore
if [ $packagesbefore == before ] || [ $packagesbefore == 0 ]
then
  sudo apt update
  echo 'Installing packages'
  sudo apt -y install html2text neofetch python3 
  sudo curl -o /usr/local/bin/googler https://raw.githubusercontent.com/jarun/googler/v4.3.2/googler && sudo chmod +x /usr/local/bin/googler
  sudo googler -u
else
  echo 'Skipping package install'
fi
crawltype() {
echo 'Would you like to crawl from google search, a dictionary list, or an encyclopedia, bible, or a superlist(dictionary, encyclopedia, and bible)?
0) Google
1) Dictionary
2) Encyclopedia
3) Bible
4) Superlist' 

read crawloption
}
if [ $crawloption == google ] || [ $crawloption == 0 ]
then
  if [ $packagesbefore != before ] || [ $packagesbefore != 0 ]
  then
    sudo apt -y install html2text
    sudo curl -o /usr/local/bin/googler https://raw.githubusercontent.com/jarun/googler/v4.3.2/googler && sudo chmod +x /usr/local/bin/googler
    sudo googler -u
  fi
  echo 'What do you want to search for? EXAMPLE: Python Challenge Answers'
  read googlesearch
  touch search.txt
  googler -C -n 10 $googlesearch | -a search.txt
  echo 'Would you like to search for a different term?
  
  Yes or No'
  read searchagain
  if [ $searchagain == yes ]
  then 
    searching() {
    echo 'What do you want to search for? EXAMPLE: Python Challenge Answers'
    read googlesearch
    touch search.txt
    googler -C -n 10 $googlesearch | -a search.txt
    echo 'Would you like to search for a different term?
    
    Yes or No'
    read searchagain
}
    searching
  fi
  #curl --get https://serpapi.com/search \
  #-d q="$googlesearch" \
  #-d location="Austin%2C+Texas%2C+United+States" \
  #-d hl="en" \
  #-d gl="us" \
  #-d google_domain="google.com" \
  #-d api_key="0190e1e7af4b221d7d69111aeed63b3e199c0bef8cd5417e35a4e12550a9f7bd" --output search.txt | html2text
elif [ $crawloption = dictionary ] || [ $crawloption = 1 ]
then
  echo 'Which dictionary should I use?
  0) Oxford
  1) Merriam-Webster
  2) All'
  echo 'I have not finished this yet'
elif [ $crawloption = encyclopedia ] || [ $crawloption = 2 ]
then
  echo "Which dictionary should I use?
  0) Children's
  1) Britannica
  2) All"
  echo 'I have not finished this yet'
elif [ $crawloption = bible ] || [ $crawloption = 3 ]
then
  echo "Which bible should I use?
  0) Christain's
  1) Jewish
  2) All"
  echo 'I have not finished this yet'
elif [ $crawloption = superlist ] || [ $crawloption = 4 ]
then
  #echo 'Installing superlist'
  echo 'I have not finished this yet'
else
  echo 'Please select an actual option'
  crawltype
fi

echo "Would you like to delete all used packages?

Yes or No"

read deltepackage

if [ $deletepakage == yes ]
then
  echo "Deleting packages"
  sudo apt remove html2text
  sudo apt remove neofetch
else
  echo "It didn't matter anyway because I have no donation methods setup."
fi
echo "All code has finished executing. Exiting"
