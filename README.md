# bash-web-crawler

A web crawler for getting Python Challenge answers. NOT FINISHED YET! DO NOT USE!

# Instruction

1. Open up the terminal
2. Type in `curl https://gitlab.com/all272/bash-web-crawler/-/raw/main/bash/main.sh --output main.sh` to download the script
3. To run the script type in `bash main.sh`

# Compatibility

Ubuntu 16 LTS, Ubuntu 18 LTS, Debian 11, and most x86 derivatives with the APT package manager.

# Sources

1. https://medium.com/swlh/how-to-modernize-your-bash-scripts-by-adding-gui-cba613a34cb7
2. https://unix.stackexchange.com/questions/285777/how-do-i-create-a-terminal-based-gui
3. https://serverfault.com/questions/707761/is-there-a-way-to-get-curl-output-into-a-file
4. https://data36.com/web-scraping-tutorial-episode-1-scraping-a-webpage-with-bash/
5. https://search.brave.com/search?q=look+for+links+in+text+bash&source=web
6. https://unix.stackexchange.com/questions/452425/compile-list-of-words-from-list-of-files
7. https://search.brave.com/search?q=bash+compile+list+of+words&source=web
8. https://www.google.com/search?q=google+scraper+bash
9. https://github.com/jarun/googler/tree/v4.3.2#installation
10. https://askubuntu.com/questions/685775/bash-get-md5-of-online-file
11. https://stackoverflow.com/questions/3679296/only-get-hash-value-using-md5sum-without-filename
12. https://stackoverflow.com/questions/4497759/how-to-get-remote-file-size-from-a-shell-script
13. https://askubuntu.com/questions/420981/how-do-i-save-terminal-output-to-a-file
